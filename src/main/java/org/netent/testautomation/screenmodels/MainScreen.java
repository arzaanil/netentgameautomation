package org.netent.testautomation.screenmodels;

import java.io.File;

import org.netent.testautomation.utils.Constants;
import org.netent.testautomation.utils.TestDataProperties;
import org.sikuli.api.ImageTarget;

public class MainScreen extends GenericScreen {
	private ImageTarget _spinButton;

	private ImageTarget _mainWindow;

	private ImageTarget _bigWinMessage;

	private ImageTarget _smallWinMessage;

	private ImageTarget _noWinMessage;

	public MainScreen() {
		super();
		//_screen.wait(getMainWindow(), TestDataProperties.getWindowTimeOut());
	}

	public ImageTarget getMainWindow() {
		_mainWindow = new ImageTarget(new File(
				TestDataProperties.path("mainWindow.png")));
		return _mainWindow;
	}

	public ImageTarget getSpinButton() {
		_spinButton = new ImageTarget(new File(
				TestDataProperties.path("spinButton.png")));
		return _spinButton;
	}

	public ImageTarget getBigWinMessage() {
		_bigWinMessage = new ImageTarget(new File(
				TestDataProperties.path("BigWinMessage.PNG")));
		return _bigWinMessage;
	}

	public ImageTarget getSmallWinMessage() {
		_smallWinMessage = new ImageTarget(new File(
				TestDataProperties.path("SmallWinMessage.PNG")));
		return _smallWinMessage;
	}

	public ImageTarget getNoWinMessage() {
		_noWinMessage = new ImageTarget(new File(
				TestDataProperties.path("NoWinMessage.PNG")));
		return _noWinMessage;
	}

	public ImageTarget getGameResultMessage(String gameResult) {
		if (gameResult.equals(Constants.NO_WIN))
			return getNoWinMessage();
		else if (gameResult.equals(Constants.SMALL_WIN))
			return getSmallWinMessage();
		else if (gameResult.equals(Constants.BIG_WIN))
			return getBigWinMessage();
		else
			return null;
	}

	public ImageTarget getImageTarget(String imagePath) {
		ImageTarget imageTarget = new ImageTarget(new File(
				TestDataProperties.path(imagePath)));
		return imageTarget;
	}

}
