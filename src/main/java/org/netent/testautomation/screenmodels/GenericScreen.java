package org.netent.testautomation.screenmodels;

import org.netent.testautomation.utils.TestDataProperties;
import org.sikuli.api.DesktopScreenRegion;
import org.sikuli.api.ImageTarget;
import org.sikuli.api.ScreenRegion;
import org.sikuli.api.robot.Keyboard;
import org.sikuli.api.robot.Mouse;
import org.sikuli.api.robot.desktop.DesktopKeyboard;
import org.sikuli.api.robot.desktop.DesktopMouse;

public abstract class GenericScreen {

	public ScreenRegion _screen;
	public Mouse _mouse;
	public Keyboard _keyboard;

	public GenericScreen() {

		_screen = new DesktopScreenRegion();
		_mouse = new DesktopMouse();
		_keyboard = new DesktopKeyboard();
	}

	protected void waitAndClick(ImageTarget target) {
		ScreenRegion sr = _screen.wait(target,
				TestDataProperties.getControlTimeOut());
		_mouse.click(sr.getCenter());
	}

	protected void waitAndDoubleClick(ImageTarget target) {
		ScreenRegion sr = _screen.wait(target,
				TestDataProperties.getControlTimeOut());
		_mouse.doubleClick(sr.getCenter());
	}

}
