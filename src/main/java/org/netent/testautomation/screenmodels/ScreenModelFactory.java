package org.netent.testautomation.screenmodels;


public class ScreenModelFactory {

	public static GenericScreen getScreen(String screenName) {

		switch (screenName) {
		
		case "main":
			return new MainScreen();

		default:
			break;
		}
		return null;

	}
}