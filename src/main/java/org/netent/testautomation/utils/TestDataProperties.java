package org.netent.testautomation.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class TestDataProperties {

	private static Properties PROPERTIES;

	static {
		PROPERTIES = new Properties();
		URL props = ClassLoader.getSystemResource("testdata.properties");
		try {
			PROPERTIES.load(props.openStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getProperty(String key) {
		return PROPERTIES.getProperty(key);
	}

	public static int getWindowTimeOut() {
		String timeout = PROPERTIES.getProperty("window.timeout");
		return Integer.parseInt(timeout);

	}

	public static int getControlTimeOut() {
		String timeout = PROPERTIES.getProperty("control.timeout");
		return Integer.parseInt(timeout);

	}

	public static String get(String key) {
		return getProperty(key);
	}

	public static String getPathFor(String file) {
		return ClassLoader.getSystemResource(file).getPath().toString();
	}

	public static File[] getSymbols() {
		String symbolsDirectoryPath = ClassLoader.getSystemResource("Symbols")
				.getPath().toString();
		File symbolsDir = new File(symbolsDirectoryPath);
		return symbolsDir.listFiles();

	}

	public static String path(String file) {
		return getPathFor(file);
	}
}
