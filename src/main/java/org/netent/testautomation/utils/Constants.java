package org.netent.testautomation.utils;

public final class Constants {

	public static final String SPIN_ACTION_NAME = "SPIN";
	public static final String NO_WIN = "No Win";
	public static final String SMALL_WIN = "Small Win";
	public static final String BIG_WIN = "Big Win";

}
