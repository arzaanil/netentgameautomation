package org.netent.testautomation.actionmodules;

import org.netent.testautomation.utils.Constants;

public class ActionFactory {

	public static AbstractAction getAction(String actionName) {

		switch (actionName) {

		case Constants.SPIN_ACTION_NAME:
			return new SpinAction();

		default:
			break;
		}
		return null;

	}
}