package org.netent.testautomation.actionmodules;

import java.io.File;
import java.util.List;

import org.netent.testautomation.screenmodels.MainScreen;
import org.netent.testautomation.screenmodels.ScreenModelFactory;
import org.netent.testautomation.utils.Constants;
import org.netent.testautomation.utils.Log;
import org.netent.testautomation.utils.TestDataProperties;
import org.sikuli.api.ImageTarget;
import org.sikuli.api.Location;
import org.sikuli.api.ScreenRegion;

public class SpinAction extends AbstractAction {

	private MainScreen _mainScreen;

	public SpinAction() {
		_mainScreen = (MainScreen) ScreenModelFactory.getScreen("main");
	}

	@Override
	public void execute() {
		executeSpinAction();
	}

	public void executeSpinAction() {
		try {
			ScreenRegion spinButton_scR = _screen.wait(
					_mainScreen.getSpinButton(),
					TestDataProperties.getControlTimeOut());
			_mouse.click(spinButton_scR.getCenter());

		} catch (Exception e) {
			Log.error("Spin Action Failed  " + e.getMessage());
		}
	}

	public String getGameResult() {
		try {

			File[] symbols = TestDataProperties.getSymbols();

			int noOfSymbolsFound = 0;

			for (File symbol : symbols) {

				ImageTarget target = new ImageTarget(symbol);

				ScreenRegion symbolScreenRegion = _screen.find(target);
				if (symbolScreenRegion != null) {
					noOfSymbolsFound++;
				}
			}

			if (noOfSymbolsFound == 1)
				return Constants.BIG_WIN;
			else if (noOfSymbolsFound == 2)
				return Constants.SMALL_WIN;
			else if (noOfSymbolsFound == 3)
				return Constants.NO_WIN;

		} catch (Exception e) {
			Log.error("Exception during matching symbols  " + e.getMessage());
		}
		return Constants.NO_WIN;
	}

	public boolean isMessageDisplayed(String gameResult) {
		try {

			ImageTarget gameResultImage = _mainScreen
					.getGameResultMessage(gameResult);

			ScreenRegion messagescreenRegion = _screen.find(gameResultImage);
			if (messagescreenRegion != null)
				return true;
			else
				return false;

		} catch (Exception e) {
			Log.error("Error during matching Message Region " + e.getMessage());
			return false;
		}
	}

}
