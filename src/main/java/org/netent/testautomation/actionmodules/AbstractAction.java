package org.netent.testautomation.actionmodules;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.netent.testautomation.utils.TestDataProperties;
import org.sikuli.api.DesktopScreenRegion;
import org.sikuli.api.ImageTarget;
import org.sikuli.api.ScreenRegion;
import org.sikuli.api.robot.Keyboard;
import org.sikuli.api.robot.Mouse;
import org.sikuli.api.robot.desktop.DesktopKeyboard;
import org.sikuli.api.robot.desktop.DesktopMouse;
import org.sikuli.api.visual.Canvas;
import org.sikuli.api.visual.DesktopCanvas;

public abstract class AbstractAction {

	public ScreenRegion _screen;
	public Mouse _mouse;
	public Keyboard _keyboard;

	public AbstractAction() {

		_screen = new DesktopScreenRegion();
		_mouse = new DesktopMouse();
		_keyboard = new DesktopKeyboard();
	}

	public abstract void execute();

	protected void clearCurrentScreenElement() {
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_A);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_BACK_SPACE);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void pressKey(int keyCode) {
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(keyCode);
			waitForControl();

		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void waitForControl() {
		try {
			Thread.sleep(TestDataProperties.getControlTimeOut());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void waitForWindow() {
		try {
			Thread.sleep(TestDataProperties.getWindowTimeOut());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void executeMousedoubleClickAction(ImageTarget target,
			ScreenRegion window, Mouse mouse) {
		ScreenRegion sr = window.wait(target,
				TestDataProperties.getControlTimeOut());
		mouse.doubleClick(sr.getCenter());
	}

	protected ScreenRegion executeMouseClickAction(ImageTarget target,
			ScreenRegion window, Mouse mouse) {
		ScreenRegion sr = window.wait(target,
				TestDataProperties.getControlTimeOut());
		mouse.click(sr.getCenter());
		return sr;
	}

	/**
	 * @param sr
	 */
	public void displayCanvas(ScreenRegion sr) {
		Canvas canvas = new DesktopCanvas();
		canvas.display(3);
	}

	public void takeScreenShot(String fileName) {
		BufferedImage image = _screen.capture();
		String userHomeDir = System.getProperty("user.home");
		File sikuliDir = new File(userHomeDir + File.separatorChar
				+ ".sikuliTestSuiteScreenShots");
		if (!sikuliDir.exists())
			sikuliDir.mkdirs();
		File screenShotDir = new File(sikuliDir.getAbsolutePath().toString()
				+ File.separatorChar + System.currentTimeMillis());
		screenShotDir.mkdirs();
		File screenShot = new File(fileName);
		try {
			ImageIO.write(image, "PNG", screenShot);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
