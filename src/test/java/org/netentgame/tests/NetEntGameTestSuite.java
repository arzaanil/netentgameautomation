package org.netentgame.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Feature", glue = { "org.netentgame.tests" },plugin = {"html:target/Destination"})

// Test cases written in Junit

 /* @RunWith(Suite.class)
  
  @SuiteClasses({ NetEntGameJunitTests.class })*/
 
public class NetEntGameTestSuite {

}
