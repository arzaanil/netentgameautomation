package org.netentgame.tests;

import org.junit.Assert;
import org.netent.testautomation.actionmodules.ActionFactory;
import org.netent.testautomation.actionmodules.SpinAction;
import org.netent.testautomation.utils.Constants;
import org.netent.testautomation.utils.Log;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class NetEntGameTestSteps extends BaseTest {

	@Given("^User launches the game$")
	public void user_launches_the_game() throws Throwable {
		openApplication();

	}

	@When("^User clicks on Spin  Wheel$")
	public void user_clicks_on_Spin_Wheel() throws Throwable {

		Log.startTestStep("Test Step Started");
		SpinAction spinAction = (SpinAction) ActionFactory
				.getAction(Constants.SPIN_ACTION_NAME);

		spinAction.execute();
		waitForControl();
		Log.endTestStep("Test Step Ended");
	}

	@Then("^display Game Result$")
	public void display_Game_Result() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Log.startTestStep("Test Step Started");
		SpinAction spinAction = (SpinAction) ActionFactory
				.getAction(Constants.SPIN_ACTION_NAME);

		String gameResult = spinAction.getGameResult();
		boolean isGameResultMessageDisplayed = spinAction
				.isMessageDisplayed(gameResult);

		Assert.assertTrue(isGameResultMessageDisplayed);

		Log.endTestStep("Test Step Ended");
	}

	@Then("^close the game$")
	public void close_the_game() throws Throwable {
		closeApplication();
	}

}
