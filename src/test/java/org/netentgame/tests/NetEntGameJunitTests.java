package org.netentgame.tests;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runners.MethodSorters;
import org.netent.testautomation.actionmodules.ActionFactory;
import org.netent.testautomation.actionmodules.SpinAction;
import org.netent.testautomation.utils.Constants;
import org.netent.testautomation.utils.Log;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NetEntGameJunitTests extends BaseTest {

	@Rule
	public TestName testName = new TestName();

	@BeforeClass
	public static void setUp() {

		openApplication();
	}

	@AfterClass
	public static void tearDown() {

		closeApplication();

	}

	@Test
	/**
	 * Test if user gets the game result on clicking the Spin Wheel Button
	 */
	public void testCase_SpinAction() {

		Log.startTestStep(testName.getMethodName());
		SpinAction spinAction = (SpinAction) ActionFactory
				.getAction(Constants.SPIN_ACTION_NAME);

		spinAction.execute();
		waitForControl();

		String gameResult = spinAction.getGameResult();
		boolean isGameResultMessageDisplayed = spinAction
				.isMessageDisplayed(gameResult);

		Assert.assertTrue(isGameResultMessageDisplayed);

		Log.endTestStep(testName.getMethodName());
	}

}
