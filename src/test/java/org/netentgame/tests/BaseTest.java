package org.netentgame.tests;

import java.util.concurrent.TimeUnit;

import org.netent.testautomation.utils.Log;
import org.netent.testautomation.utils.TestDataProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BaseTest {

	public static WebDriver driver = null;

	protected static void openApplication() {

		try {
			String sBrowserName;

			sBrowserName = TestDataProperties.get("application.browser");
			if (sBrowserName.equals("InternetExplorer")) {
				driver = new InternetExplorerDriver();
			} else if (sBrowserName.equals("GoogleChrome")) {
				driver = new ChromeDriver();
			} else if (sBrowserName.equals("Firefox")) {
				driver = new FirefoxDriver();
			}

			Log.info("New driver instantiated");

			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			Log.info("Implicit wait applied on the driver for 10 seconds");
			driver.navigate().to(TestDataProperties.get("application.url"));
			Log.info("Web application launched successfully");

		} catch (Exception e) {
			Log.fatal("Failed to Launch NetEnt Game Application "
					+ e.getMessage());

			System.exit(-1);
		}

	}

	protected static void closeApplication() {

		driver.quit();
	}

	public static void waitForControl() {
		try {
			Thread.sleep(TestDataProperties.getControlTimeOut());
		} catch (InterruptedException e) {
			Log.error("Thread Interrupted " + e.getMessage());
		}
	}

	public static void waitForWindow() {
		try {
			Thread.sleep(TestDataProperties.getWindowTimeOut());
		} catch (InterruptedException e) {
			Log.error("Thread Interrupted " + e.getMessage());
		}
	}

}
