#NetEnt Game Test Automation#


## **Description** ##

NetEnt Game Test Automation is project designed to automate test cases of spin game functionalities. In this project , a test automation Framework has been designed using Sikuli Java API which helps us to design different automation test cases. The project uses maven as build system to build, manage the dependencies and execute the tests. The test cases are developed using Cucumber.Also the project uses Junit which invokes Cucumber Test Runner for test execution.. Test Cases are designed both in Junit and Cucumber. The project uses maven as build system to build, manage the dependencies and execute the tests.

 
## Environment ##

The project uses the following tools and APIs which is defined in pom.xml

* JUnit - 4.11
* Cucumber - 1.2.5
* sikuli-api  - 1.2.0 
* Selenium- 2.47.1
* log4j - 1.2.17 
* java – 1.7 or higher


Note : There are no platform specific attributes used in Test Cases or Test Framework API. But the test cases are only verified on Windows OS. 



## **Directory Structure** 

 **src/main :**       contains different modules in test automation framework. Test  Automation Framework is implemented in a module driven approach. Please find the architecture guide in the docs folder
      	      	    
    	- src/main/java : contains the implementation of actions and screen modules in the framework using Sikuli Java API
      		
              - utils : package under src/main/java which contains all the utility methods used by the framework like Loggers,Constants and TestDataProperties.

    	- src/main/resources  : ### contains all the resources i.e., required images files used by Sikuli to match patterns for the user actions.  

src/test :   contains all the test cases and resource files are present in this package.
    	
    	 - src/test/java :   contains all the test scripts along with JUnit Test suites.All the Cucumber Feature Scenarios are mapped as Test Steps in this package.
             	
      		**- utils :** package under src/test/java which contains all the utility methods used by the test scripts.

    	- src/test/resources  :   contains all the resources i.e., required images files used by Sikuli to match patterns for the test cases. Also, a property file testdata.properties is maintained to handle test data for test cases.

Feature :   This folder contains all the Cucumber feature files which contains Scenarios written using  Gherkin Keywords. These scenarios will be mapped to Test Steps implemented in src/test/java

Lib :         contains the libraries required to ececute tests.java cpp native libraries required for sikuli API. This folder contains javacpp	folder which contains java cpp native libraries for Windows 64 bit OS. If you would like to run tests on different OS, please download the respective java cpp libraries and keep them in lib directory.
    



## **Steps to run the Test Cases ** 


## TestSuite Class: ##

Test cases are implemented using both Junit and Cucumber. 

NetEntGameTestSuite.java -  This test suite will execute all the scenarios present in feature files using Cucumber Test Runner


#### Prerequisite to run test cases: ###

This step is required if you use OS other than Windows x86 64 bit or if you want to run tests using Junit Runner.

Since the test cases are designed using Sikuli Java API, we need Java CPP native libraries. So, we need to set java.library.path to the folder containing javacpp native libraries for the corresponding OS environment. For Windows 64 bit OS , these libraries are already present in lib directory under project. To run Junit Tests, please specify the below system property in Run Configuration under VM arguments.

-Djava.library.path="<projectdirectory>\lib\javacpp"

 Please note that these libraries are already present in lib folder for Windows 64-bit OS and the above property has been set in Maven POM. So, if you run tests from Maven, no need to set this property.

### Steps ###


Please find the steps to run Test Cases below:

1. Clone and execute Maven Clean and Build**
     
      Using IDE : 
     Clone the repository and Import the Project into any IDE (Eclipse etc..) and execute maven clean and build. Please ensure that all the maven dependencies are installed and added to classpath. In case  of Eclipse IDE, If we are using m2e plugin, it will automatically update the classpath. 
 
     Using Maven Command Line :
     This step can also be done with maven commandline using maven lifecycle commands
 
2. Set the browser path in which the tests shall be run in testdata.properties file.By default the tests will be run in InternetExplorer. Open testdata.properties located under src/test/resources package and Edit the value for the key 'application.browser. Since, the tests using Selenium to launch the Game URL on a particular browser , make sure that the corresponding Web Driver is Installed and set in PATH variable.
Note : Currently , the tests only support InternetExplorer, GoogleChrome and FireFox Browsers. But they can be extended to support other browsers also. 
    
 
4.  Execute the Test Cases
 
      TesTs can be executed in the following ways. 
      
      Using Maven approach is the easiest and preferred approach. NetEntGameTestSuite.java file is specified in surefire plugin configuration of Maven POM xml file. 

 		1) Using Maven (Preferred)
 		
 		  From IDE (Eclipse etc)-  If the maven build is successful and all the dependencies are resolved in the above steps, just Run Maven test from Run Configurations. 
 
                From Command line -   using the Maven Build system, we can execute using 						command mvn test.
            
             2) Using Junit Plugin From IDE (Eclipse,etc..)       
     
              The TestSuites can be launched and executed through Junit RunConfiguration from any IDE (Eclipse,etc..). RightClick on any of the above mentioned TestSuite class file and RunAs Junit Tests. Also, the test classes mentioned in the suites can be executed as a seperate Junit Tests.
                     
                          
5. Check the status of test results in Cucumber Test Reports generated in Destination folder created under target directory. Also, the status of tests can be seen in the Log file located in log folder under project directory. A TestListener is implemented and Maven will invoke that during tests(Not Integrated in Maven).


## Improvements or Further Extensions ##

Currently a few tests has been designed and implemented but we can extend the test suite by adding more tests for the game. 

* * There can be many variants of tests that can be designed for different scenarios like negative tests,exception  tests,different error scenarios and data driven tests. But for this assignment, I have designed only basic level of test cases to verify the main functionality.These test classes can be extended to perform data driven and Regression Testing.

* * As the game uses Random Generator function , we can mock (mimic) the random generator behavior and execute the spin test case some random number of times to check if the random generator works as expected

* * We can execute the test suite on multiple browsers paralelly by using Data Driven Framework. Currently, a properties file is used to maintain test data or application properties. This can be replaced with Excel file and Using Apache POI API, we can perform tests with several data.


## Assumptions ##

*  All the dependencies like Sikuli,Selenium,Cucumber and the JUnit mentioned in Maven POM file shall be added to classpath. If we use maven m2e plugin , it will automatically update and adds the dependencies to classpath. 
*  Maven Home and correct Java Version (java 1.7+) is installed and correctly configured.The test cases assumes that the game back-end functionalities(Random Generator,etc..) are working at the time when the test case runs.
*  **Timeouts are configured in testdata.properties file. I have assumed that Opening of Game URL require 5 sec and control opertions require 3 sec. Please change these timeouts based on the network speed and other performance criterias if any.** 
*  Also, I have designed a test automation framework using Module Driven Architecture Guidelines. This framework can be easily extended, maintainable,reusable and assists to design several test cases in an easy, efficient way with less effort.


### Documents or References ###


Please find the below documents in doc folder under project directory.

* TestFrameworkArchitectureGuide -  Document that describes the test automation framework architecture , its goals, principle,  modules and class diagrams